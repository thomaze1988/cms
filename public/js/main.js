const pages = document.getElementById('pages');

if(pages){
    pages.addEventListener('click', (e) => {
        if( e.target.className === 'btn btn-danger delete-page'){
            e.preventDefault(); // tenzij je de standaard actie van een href blokkeert gaat ie er nog gewoon heen

            if (confirm('Are you sure you want to delete this page?')){
                const id = e.target.getAttribute('data-id');

                fetch(`/public/index.php/delete/${id}`, {
                    method: 'DELETE'
                }).then(res => window.location.reload());
            }
            return false;
        }
    });
}