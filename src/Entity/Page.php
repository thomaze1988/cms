<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=48, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $menuPos;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $bannerImgUrl;

    // subtitel per textveld toevoegen?
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textArea;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $contentImageUrl;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textArea2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getMenuPos()
    {
        return $this->menuPos;
    }

    /**
     * @param mixed $menuPos
     */
    public function setMenuPos($menuPos): void
    {
        $this->menuPos = $menuPos;
    }

    /**
     * @return mixed
     */
    public function getBannerImgUrl()
    {
        return $this->bannerImgUrl;
    }

    /**
     * @param mixed $bannerImgUrl
     */
    public function setBannerImgUrl($bannerImgUrl): void
    {
        $this->bannerImgUrl = $bannerImgUrl;
    }

    /**
     * @return mixed
     */
    public function getTextArea()
    {
        return $this->textArea;
    }

    /**
     * @param mixed $textArea
     */
    public function setTextArea($textArea): void
    {
        $this->textArea = $textArea;
    }

    /**
     * @return mixed
     */
    public function getContentImageUrl()
    {
        return $this->contentImageUrl;
    }

    /**
     * @param mixed $contentImageUrl
     */
    public function setContentImageUrl($contentImageUrl): void
    {
        $this->contentImageUrl = $contentImageUrl;
    }

    /**
     * @return mixed
     */
    public function getTextArea2()
    {
        return $this->textArea2;
    }

    /**
     * @param mixed $textArea2
     */
    public function setTextArea2($textArea2): void
    {
        $this->textArea2 = $textArea2;
    }


}
