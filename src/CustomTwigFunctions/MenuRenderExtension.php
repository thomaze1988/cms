<?php

namespace App\CustomTwigFunctions;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\Page;


class MenuRenderExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    protected $doctrine;

    /**
     * MenuRenderExtension constructor.
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
          new TwigFunction('showMenu', array($this, 'generateMenu'))
        );
    }

    public function generateMenu()
    {
        /** @var Page[] $list */
        $list = $this->doctrine->getRepository(Page::class)->findAll();
        $array = array();

        foreach($list as $entry){
            if($entry->getMenuPos() > 0)
            {
                $array[$entry->getMenuPos()] = array("pos" => $entry->getMenuPos(), "title" => $entry->getTitle(), "id" => $entry->getId());
            };
        }

        ksort($array);
        return $array;
    }
}

