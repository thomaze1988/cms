<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use App\Entity\Page;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ConfigScreenController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Method({"GET"})
     */
    public function index()
    {
        return $this->render('pages/index.html.twig');
    }

    /**
     * @Route("/management", name="management")
     * @Method({"GET"})
     */
    public function management()
    {
        $pages = $this->getDoctrine()->getRepository(Page::class)->findAll();
        return $this->render('pages/management.html.twig', array('pages' => $pages));
    }

    /**
     * @Route("/page/new", name="new_page")
     * @Method({"GET", "POST"})
     */
    public function new(Request $request){
        $page = new page();

        $form = $this->createFormBuilder($page)
            ->add('title',              TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('menuPos',            ChoiceType::class, array('attr' => array('class' => 'form-control'), 'choices' => array('Verbergen' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10, 'Maak home' => 40000 )))
            ->add('bannerImgUrl',       TextType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('textArea',           TextAreaType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('contentImageUrl',    TextType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('textArea2',          TextAreaType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('save',               SubmitType::class, array('label' => 'Create','attr' => array('class' => 'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $page = $form->getData();

            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $bannerFile */
            //$bannerFile = $page->getbannerImgUrl();
           // $bannerFileName = $this->generateUniqueFileName().'.'.$bannerFile->guessExtension();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($page);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }
        return $this->render('pages/new.html.twig', array('form'=> $form->createView()));
    }

    /**
     * @Route("/delete/{id}")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id){
        $page = $this->getDoctrine()->getRepository(Page::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($page);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }


    /**
     * @Route("/contact", name="contact")
     * @Method({"GET", "POST"})
     */
    public function contact(){
        return $this->render('pages/contact.html.twig');
        // hier moet nog een standaard contact form bij
    }

    /**
     * @Route("/page/{id}")
     */
    public function showPage($id)
    {
        $page = $this->getDoctrine()->getRepository(Page::class)->find($id);

        return $this->render('pages/show.html.twig', array('page' => $page));
    }

    /**
     * @Route("/edit/{id}", name="edit_page")
     * @Method({"GET", "POST"})
     */
    public function edit(Request $request, $id){
        $page = $this->getDoctrine()->getRepository(Page::class)->find($id);

        $form = $this->createFormBuilder($page)
            ->add('title',              TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('menuPos',            ChoiceType::class, array('attr' => array('class' => 'form-control'), 'choices' => array('Verbergen' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10, 'Maak home' => 40000 )))
            ->add('bannerImgUrl',       TextType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('textArea',           TextAreaType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('contentImageUrl',    TextType::class, array('required' => false,'attr' => array('class' => 'form-control')))
            ->add('textArea2',          TextAreaType::class, array('required' => false,'attr' => array('class' => 'form-control')))
//            ->add('bannerImgUrl',              FileType::class, array('label' => 'Image file'))
            ->add('save',               SubmitType::class, array('label' => 'Update','attr' => array('class' => 'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }
        return $this->render('pages/edit.html.twig', array('form'=> $form->createView()));
    }
}
